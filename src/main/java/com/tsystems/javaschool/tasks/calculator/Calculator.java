package com.tsystems.javaschool.tasks.calculator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null || statement.equals("")) return null;
        if (statement.matches(".*(,|//).*")) return null;

        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");

        Object result;
        try {
            result = engine.eval(statement);
            if (result.getClass().equals(Integer.class)) {
                return ((Integer) result).toString();
            } else if (result.getClass().equals(Double.class) && !((Double) result).isInfinite()) {
                return result.toString();
            } else {
                return null;
            }
        } catch (ScriptException e) {
            return null;
        }
    }
}
