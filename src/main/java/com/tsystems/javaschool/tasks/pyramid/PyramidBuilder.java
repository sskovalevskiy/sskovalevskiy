package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.contains(null) || !hasEnoughElements(inputNumbers)) throw new CannotBuildPyramidException();

        Collections.sort(inputNumbers);
        int amountOfRows = (int) (-1 + Math.sqrt(1 + 8 * inputNumbers.size())) / 2;
        int amountOfZerosInRow = amountOfRows - 1;
        int amountOfElementsInRow = amountOfRows + amountOfZerosInRow;

        int[][] resultArray = new int[amountOfRows][amountOfElementsInRow];

        int inputNumbersListIndex = 0;
        for (int i = 0; i < amountOfRows; i++) {
            for (int j = (amountOfElementsInRow - (2 * i + 1)) / 2; j < (amountOfElementsInRow + 2 * i + 1) / 2; j = j + 2) {
                resultArray[i][j] = inputNumbers.get(inputNumbersListIndex);
                inputNumbersListIndex++;
            }
        }
        return resultArray;
    }

    /**
     * Finds if given List has enough elements for building Pyramid.
     * Each next line has plus one element(number) to previous. We get arithmetic progression for amount of numbers in a row:
     * (1);(2,3);(4,5,6);(7,8,9,10)... so on. If counted numberOfRows is Integer - given list has enough elements, else - not.
     *
     * @param inputNumbers - given list of numbers
     * @return true, if inputNumbers has enough elements for building pyramid, else - false.
     */
    private boolean hasEnoughElements(List<Integer> inputNumbers) {
        double amountOfRows = (-1 + Math.sqrt(1 + 8 * inputNumbers.size())) / 2;
        return (amountOfRows % 1 == 0);
    }
}
